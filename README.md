Antragsidee: HS Merseburg Pluslab

# Thema

Datum: 02.04.20 (Bekanntmachung)

_„Interaktive Systeme in virtuellen und realen Räumen – Innovative Technologien für die digitale Gesellschaft“_

## Grundlage / Idee

BMBF (Bundesministerium für Bildung und Forschung) -Forschungsprogramms zur Mensch-Technik-Interaktion (MTI) „Technik zum Menschen bringen“

- neue Qualität der **zwischenmenschlichen Kommunikation** und des **Kompetenzerwerbs über Distanzen hinweg** oder auch oder auch für **motorisches Training** ermöglicht werden
- verbesserte Immersion (Erweiterung der visuellen und auditiven Wahrnehmung)
- Multi-User-Anwendungen
- Beteiligung von **Start-ups** und **KMUs**

## Gegenstand der Förderung

- innovative Forschungs- und Entwicklungsvorhaben der **Mensch-Technik-Interaktion** (MTI) im Bereich _"Digitale Gesellschaft"_
- Technologien für eine **gemischte Realität** („Mixed Reality“ – MR), einschließlich virtueller (VR) und erweiterter Realität („Augmented Reality“ – AR) um physische Interaktionsmöglichkeiten ergänzen.
- Usability
- Echtzeitfähigkeit
- Nachhaltigkeit
- **interdisziplinäre** Zusammenarbeit zwischen Natur-, Geistes- und ­Betriebswissenschaften

### Herausforderungen (Vorschläge)

| multimodale Interaktionstechniken                                                                                                                                                                                                                                                                                                    | Multi-User                                                                                                                                                                                                                              | Usability/Akzeptanzverbesserung                                                                                                                                                                                                                                                                                                                                                                                                                            |
| ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Neue Haptik-/Taktilitätskomponenten zur Wahrnehmung unterschiedlicher Oberflächen, Formen und Gewichte, aber auch solchen Komponenten zur Wahrnehmung von anderen Sinneseindrücken wie beispielsweise Temperaturen und Gerüchen, bis hin zu Fragen zur Erkennung und Darstellung von Emotionen sowie beispielsweise Mimik und Gestik | Gleichzeitige, möglichst echtzeitnahe Positionsbestimmung mehrerer Personen und durch Methoden zur Synchronisation der Bewegungen zwischen realer und virtueller Welt, um eine bestmögliche soziale Interaktion zu erreichen            | Auswirkungen auf eine Gesellschaft, in der sich Menschen nicht direkt, sondern rein virtuell begegnen, sollten nicht nur im Rahmen der ELSI-Betrachtungen (ethische, rechtliche und soziale Implikationen) untersucht werden. Hierzu ist insbesondere auch die Erforschung von Effekten der Nutzung über einen längeren Zeitraum hinweg relevant.                                                                                                          |
| Neue 3D-Eingabegeräten und -techniken zur intuitiven Steuerung von MR-Systemen. Dies können neben ­controllergebundenen Technologien auch Verfahren zur Gesten- und Sprachsteuerung sowie auch neuronale Schnittstellen sein.                                                                                                        | Untersuchungen der gelungenen virtuellen Abbildung der Nutzerinnen und Nutzer. Hier stehen neben Fragen zur Darstellung von individuellen 3D-Charakteren auch die Interaktionen zwischen mehreren ­Avataren aber auch Agenten im Fokus. | Die Potenziale der anschaulichen und direkt erlebbaren Vermittlung von komplexem Wissen müssen durch ­Evaluationen überprüft werden. Es sollte nachgewiesen werden, inwieweit die neu entwickelten Systeme tatsächlich Vorteile gegenüber bisher üblichen Methoden und Technologien zum Wissenserwerb bieten und untersucht werden, wie MR-Systeme geschaffen sein müssen, damit virtuelles Lernen auch erfolgreich ist.                                   |
|                                                                                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                         | Die ständige und anschauliche Verfügbarkeit von Information kann ebenso Auswirkungen auf eine Gesellschaft und deren Bezug zum Thema Wissen haben, Auswirkungen beim plötzlichen Fehlen einer solchen Verfügbarkeit oder zur Filterung von falschen Informationen können weitere Forschungsfragen sein.                                                                                                                                                    |
|                                                                                                                                                                                                                                                                                                                                      |                                                                                                                                                                                                                                         | Die einfache und mobile Verfügbarkeit der entwickelten Lösungen sowie die Finanzierbarkeit und Nachhaltigkeit von deren Entwicklung und Betrieb sind weitere wichtige Faktoren, die es für einen breitenwirksamen Einsatz von MR-Technologien zu berücksichtigen gilt. Hierzu sollten bereits während der Konzeption insbesondere auch Aspekte der Energie- und Ressourceneffizienz der entwickelten Systeme grundlegend betrachtet und diskutiert werden. |

### Themenfelder

- Immersive und effektive Wissensräume
- Soziale und kulturelle Teilhabe

## Module

1. Forschungs- und Entwicklungsfokus
   - technische Umsetzung
   - Alltagstauglichkeit
   - sozialen und gesellschaftlichen Nutzen in Studien nachweisen
   - User Centered Design
   - interdisziplinär
   - ELSI
   - Kooperation mit Living Lab einplanen
   - erheblichen Forschungsbedarf aufweisen
   - Förderlaufzeit: **3 Jahre**
2. Living Lab

   - verbundübergreifende Zusammenarbeit der Projekte
   - praxisnahe Evaluation der einzelnen Entwicklungen
   - Förderlaufzeit: **3 Jahre**

# Zuwendungsempfänger

- Verbünde aus Unternehmen der Wirtschaftt, Hochschulen, außeruniversitäre Forschungseinrichtungen, zivilgesellschaftliche Akteure
- Start-ups, KMUs
- ein Koordinator zu benennen
- Vorhandensein einer Betriebsstätte in Germany
- Förderungen für vom Bund/Land grundfinanzierten Einrichtungen _eingeschränkt_

# Art und Umfang, Höhe der Zuwendung

- bis zu 50% für wirtschaftliche Partner
- bis zu 100% für nicht wirtschaftliche Partner
- Bei nichtwirtschaftlichen Forschungsvorhaben an Hochschulen und Universitätskliniken wird zusätzlich zu den ­zuwendungsfähigen Ausgaben eine Projektpauschale in Höhe von 20 % gewährt.
- Modul 1: technische Umsetzung und Realisierung der Projektideen
- Module 2: der Aufbau und der Betrieb von mindestens einem Living Lab.

# Fristen

- bis spätestens zum 29. Juli 2020 12 Uhr mittags muss eine Skizze vorliegen
